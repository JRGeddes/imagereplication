﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace ImageReplication
{
		class Program
		{
			static string strDBConString = string.Empty;
			static string strPrimaryImageSharedStoragePath = string.Empty;
			static string strStandByImageSharedStoragePath = string.Empty;
			static int nAWSRETSID = 0;

			static void Main(string[] args)
			{
				try
				{
					if (args.Length == 0)
					{
						Console.WriteLine("Usage: ImageReplication {AWSRETSID}");
						return;
					}

					int nPrimaryMissingImages = 0;
					int nTotalImages = 0;
					int nListings = 0;
					int nSLCMissingImagesFixed = 0;
					bool bPrimaryMissingImage = false;
					bool bStandByMissingImage = false;

					nAWSRETSID = Convert.ToInt16(args[0]);
					bool Active;
					bool DownloadImages;

					Log("Starting");

					if (!GetBoardInfo(nAWSRETSID, out strDBConString, out Active, out DownloadImages))
					{
						Log("There is a problem with the AWSRets record for this board");
					}
					else if (!Active)
					{
						Log("This board isn't active");
					}
					else if (!DownloadImages)
					{
						Log("Images aren't downloaded for this board");
					}
					else
					{
						SqlConnection con = null;
						try
						{
							con = new SqlConnection(strDBConString);
							con.Open();
							using (SqlDataAdapter a = new SqlDataAdapter("SELECT AWSRListingID, MLSListingKey, AWSRListingTypeID, MLSNumber FROM dbo.AWSRListings (NOLOCK) ORDER BY AWSRListingID", con))
							{
								a.SelectCommand.Parameters.Add("@AWSRETSID", SqlDbType.Int).Value = nAWSRETSID;
                                a.SelectCommand.CommandTimeout = 90;
								DataTable dt = new DataTable();
								a.Fill(dt);

								Log(dt.Rows.Count.ToString() + " listings to verify");
								foreach (DataRow r in dt.Rows)
								{
									int nAWSRListingID = Convert.ToInt32(r["AWSRListingID"]);
									string strImagePath = GetImagePath(nAWSRListingID);

									DataTable dtImages = GetListingImages(nAWSRListingID);
									foreach (DataRow rImage in dtImages.Rows)
									{
										string strImageName = r["MLSListingKey"].ToString() + "_" + rImage["SortOrder"].ToString() + ".jpg";
										if (nAWSRETSID == 39 || nAWSRETSID == 5)
										{
											strImageName = r["MLSNumber"].ToString() + "_" + rImage["SortOrder"].ToString() + ".jpg";
										}

										//parse the MLSID out of the URL because some boards are stored in the same db but have multiple AWSRets entries
										string strMLSID = rImage["MediaName"].ToString();
										if (!strMLSID.StartsWith("http://"))
										{

                                        try
                                            {
                                                strMLSID = strMLSID.Substring(strMLSID.IndexOf("rID=") + 4, 3);
                                            }
                                            catch
                                            {
                                                Log("Could not parse MLSID for " + strMLSID);

                                            }

											

                                            if (strMLSID.IndexOf("&") >= 0)
											{
												int nPos = strMLSID.IndexOf("&");
												strMLSID = strMLSID.Substring(0, nPos);
											}

											string strPrimaryFile = Path.Combine(strPrimaryImageSharedStoragePath, Path.Combine(strMLSID, Path.Combine(strImagePath, strImageName)));
											string strStandByFile = Path.Combine(strStandByImageSharedStoragePath, Path.Combine(strMLSID, Path.Combine(strImagePath, strImageName)));
											if (!File.Exists(strStandByFile))
											{
												if (!File.Exists(strPrimaryFile))
												{
                                                    //JRG 2/22/2016 - commenting this out to see how much quicker the app runs	
                                                    //Log("Missing image for AWSRListingID: " + nAWSRListingID.ToString() + " " + strPrimaryFile);
                                                    nPrimaryMissingImages += 1;
													bPrimaryMissingImage = true;
												}
												else
												{
													//see if the secondary dir exists
													string strStandByPath = Path.Combine(strStandByImageSharedStoragePath, Path.Combine(strMLSID, strImagePath));
													if (!Directory.Exists(strStandByPath)) Directory.CreateDirectory(strStandByPath);

													//copy the file from Primary
                                                    //JRG 5/9/2016 - added the true so that the app won't bomb if the file already exists in the destination.
													File.Copy(strPrimaryFile, strStandByFile,true);
													bStandByMissingImage = true;
													nSLCMissingImagesFixed += 1;
                                                    //JRG 2/22/2016 - commenting this out to see how much quicker the app runs
													//Log(strStandByFile + " copied from primary to secondary");
												}
											}

											nTotalImages += 1;
										}
									}

									if (bPrimaryMissingImage)
									{
                                        //JRG 2/22/2016 - commenting this out to see how much quicker the app runs	
                                        //LogMissingImage(nAWSRETSID, nAWSRListingID, Convert.ToInt32(r["AWSRListingTypeID"]), r["MLSNumber"].ToString());
                                        bPrimaryMissingImage = false;
									}

									nListings += 1;

									if (Properties.Settings.Default.Vervose)
									{
										Console.Write("\r{0} {1}", nListings, (Convert.ToDecimal(nListings) / Convert.ToDecimal(dt.Rows.Count)).ToString("##.00%"));
									}
								}

								Log(nTotalImages.ToString() + " total images to verify");
								if (nPrimaryMissingImages == 0)
								{
									Log("No missing images for this board");
								}
								else
								{
									Log(nPrimaryMissingImages.ToString() + " missing images for this board");
								}

								if (bStandByMissingImage)
								{
									Log(nSLCMissingImagesFixed.ToString() + " images were copied to StandBy");
									if (nSLCMissingImagesFixed > Properties.Settings.Default.ReplicationVarianceCount || (nSLCMissingImagesFixed/nTotalImages * 100) > Properties.Settings.Default.ReplicationVariancePercentage)
									{
										//we had to copy too many files to the StandBy server.  Send an email
										System.Net.Mail.SmtpClient oSMTP = new System.Net.Mail.SmtpClient();
										System.Net.Mail.MailMessage oMail = new System.Net.Mail.MailMessage(Properties.Settings.Default.ExceptionMailFrom, Properties.Settings.Default.ReplicationVarianceEmail);
										oMail.Subject = "ImageReplication Variance for Board - " + nAWSRETSID.ToString();
										oMail.Body = nSLCMissingImagesFixed.ToString() + " images out of " + nTotalImages.ToString() + " were copied to the standby server.";
										oSMTP.Host = Properties.Settings.Default.SMTPServer;
										oSMTP.Send(oMail);
									}
								}
							}
						}
						finally
						{
							if (con != null) { con.Close(); con.Dispose(); }
						}
                    Log("Image Sync for board " + nAWSRETSID.ToString() + " is complete");
					}
				}
				catch (Exception ex)
				{
					System.Net.Mail.SmtpClient oSMTP = new System.Net.Mail.SmtpClient();
					System.Net.Mail.MailMessage oMail = new System.Net.Mail.MailMessage(Properties.Settings.Default.ExceptionMailFrom, Properties.Settings.Default.ExceptionMailTo);
					oMail.Subject = "ImageReplication Failed for Board - " + nAWSRETSID.ToString();
					oMail.Body = ex.ToString();
					oSMTP.Host = Properties.Settings.Default.SMTPServer;
					oSMTP.Send(oMail);

					Log(ex.ToString());
				}
			}

			static DataTable GetListingImages(int nAWSRListingID)
			{
				using (SqlConnection con = new SqlConnection(strDBConString))
				{
					con.Open();
					using (SqlDataAdapter a = new SqlDataAdapter("SELECT SortOrder, MediaName FROM dbo.AWSRListingMedia (NOLOCK) WHERE AWSRListingMediaTypeID = 1 AND AWSRListingID = @ID AND MediaName NOT LIKE '%nolistingphoto.jpg' ORDER BY SortOrder", con))
					{
						a.SelectCommand.Parameters.Add("@ID", SqlDbType.Int).Value = nAWSRListingID;
						DataTable dt = new DataTable();
						a.Fill(dt);
						return dt;
					}
				}
			}

			static string GetImagePath(int nAWSRListingID)
			{
				using (SqlConnection con = new SqlConnection(strDBConString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand("dbo.spGetImagePath", con))
					{
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.Parameters.Add("@AWSRListingID", SqlDbType.Int).Value = nAWSRListingID;
						return cmd.ExecuteScalar().ToString();
					}
				}
			}

			static bool GetBoardInfo(int nAWSRETSID, out string DBCon, out bool Active, out bool DownloadImages)
			{
				using (SqlConnection con = new SqlConnection(Properties.Settings.Default.IDXDataCon))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand("SELECT IDXServer, IDXDatabase, ImageSharedStoragePath, RETS_Active, ImageURLGiven FROM IDXCommon.dbo.AWSRETS (NOLOCK) WHERE id = @ID", con))
					{
						cmd.Parameters.Add("@ID", SqlDbType.Int).Value = nAWSRETSID;
						SqlDataReader dr = cmd.ExecuteReader();
						if (dr.Read())
						{
							strPrimaryImageSharedStoragePath = dr["ImageSharedStoragePath"].ToString();
							SetDRPath();
							DBCon = "Data Source=$Server#;Initial Catalog=$DB#;USER ID=IDXServer;Password=Ml$wr1t3r;Application Name=ListingImageVerify".Replace("$Server#", dr["IDXServer"].ToString()).Replace("$DB#", dr["IDXDatabase"].ToString());
							Active = Convert.ToBoolean(dr["RETS_Active"]);
							DownloadImages = !Convert.ToBoolean(dr["ImageURLGiven"]);
							return true;
						}
						else
						{
							Active = false;
							DownloadImages = false;
							DBCon = string.Empty;
							return false;
						}
					}
				}
			}

			static void SetDRPath()
			{
				string[] aOKCPaths = Properties.Settings.Default.PrimaryLocations.Split(';');
				string[] aSLCPaths = Properties.Settings.Default.StandByLocations.Split(';');

				strStandByImageSharedStoragePath = strPrimaryImageSharedStoragePath;
				for (int i = 0; i < aOKCPaths.Length; i++)
				{
					strStandByImageSharedStoragePath = strStandByImageSharedStoragePath.Replace(aOKCPaths[i], aSLCPaths[i]);
				}
			}

			static void Log(string strMsg)
			{
				StreamWriter objSW = File.AppendText(Properties.Settings.Default.LogFile.Replace("$RETSID#", nAWSRETSID.ToString()));
				objSW.WriteLine(System.DateTime.Now + "|" + strMsg);
				objSW.Close();

				Console.WriteLine("\r" + strMsg);
			}

			static void LogMissingImage(int nAWSRETSID, int nAWSRListingID, int nAWSRListingTypeID, string strMLSNumber)
			{
				using (SqlConnection con = new SqlConnection(Properties.Settings.Default.IDXDataCon))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand("INSERT INTO dbo.MissingListingImages (AWSRETSID,AWSRListingID,MLSNumber,AWSRListingTypeID) VALUES ( @AWSRETSID,@AWSRListingID,@MLSNumber, @AWSRListingTypeID)", con))
					{
						cmd.Parameters.Add("@AWSRETSID", SqlDbType.Int).Value = nAWSRETSID;
						cmd.Parameters.Add("@AWSRListingID", SqlDbType.Int).Value = nAWSRListingID;
						cmd.Parameters.Add("@AWSRListingTypeID", SqlDbType.Int).Value = nAWSRListingTypeID;
						cmd.Parameters.Add("@MLSNumber", SqlDbType.VarChar).Value = strMLSNumber;
						cmd.ExecuteNonQuery();
					}
				}
			}
		}
	}
